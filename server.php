<?php
session_start();

if (isset($_POST['login'])) {
    $username = $_POST['email'];
    $password = $_POST['password'];
    if ($username === 'johnsmith@gmail.com' && $password === '1234') {
        $_SESSION['email'] = $username;
    } else {
        header('Location: ./index.php');
        exit();
    }
}

if (isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    header('Location: ./index.php');
    exit();
}
if (isset($_SESSION['email'])) {
    header('Location: ./index.php');
    exit();
}

?>
