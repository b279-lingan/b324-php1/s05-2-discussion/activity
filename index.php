<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Activity s05</title>
</head>
<body>
    <h1>Login Page</h1>
    <?php
    session_start();
    if (isset($_SESSION['email'])) {
    ?>
        <p>Hello🤸‍ <?php echo $_SESSION['email']; ?></p>
        <form method="POST" action="./server.php">
            <button type="submit" name="logout">Logout</button>
        </form>
    <?php
    } else {
    ?>
        <form method="POST" action="./server.php">
            <label for="email">Email:</label>
            <input type="email" name="email" id="email" required>
            <label for="password">Password:</label>
            <input type="password" name="password" id="password" required>
            <button type="submit" name="login">Login</button>
        </form>
    <?php } ?>
</body>
</html>

